var express = require('express');
var data = require('./user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
const global = require('./global');

const user_controller = require("./Controllers/users_controller.js");
const auth_controller = require('./Controllers/auth_controller');
app.use(bodyParser.json());

//perticion GET  user con ID;
app.get(global.URL_BASE + "users/:id", user_controller.getById);

app.get(global.URL_BASE + "users", user_controller.getQuerys);

app.get(global.URL_BASE + "users", user_controller.getUsers);
// POST users
// app.post(URL_BASE + 'users', function(req, res){
//   console.log("POST");
//   //res.send("POST Success");
//   console.log(req.body.first_name);
//   console.log(req.body.email);

//   let newPos = data.length + 1;
//   let newUser = {
//     "id": newPos,
//     "first_name": req.body.first_name,
//     "last_name": req.body.last_name,
//     "email": req.body.email,
//     "password": req.body.password
//   }
//   data.push(newUser);
//   res.send(newUser);
//   console.log(newPos);
// });

// app.put(URL_BASE + 'users/:id', function(req, res){
//   console.log("PUT success");
//   let pos = req.params.id-1;
//   let rspt = data[pos];
// })

//PUT users
app.put(global.URL_BASE + "users/:id", function(req, res) {
  let editUser = {
    id: req.params.id,
    first_name: req.body.first_name,
    last_name: req.body.last_name,
    email: req.body.email,
    password: req.body.password
  };
  data[req.params.id - 1] = editUser;
  // console.log("Ediciòn usuario: " + editUser);
  res.send(editUser);
});

//DELETE users
app.delete(global.URL_BASE + "users/:id", function(req, res) {
  let pos = req.params.id;
  data.splice(pos - 1, 1);
  res.send({
    msg: "Usuario eliminado"
  });
});

//LOGIN users
app.post(global.URL_BASE + "users", auth_controller.Login);

//LOGOUT users
app.post(global.URL_BASE + "users/:id", auth_controller.Logout);

app.listen(port, function() {
  console.log("Example app listening on port 3000!");
});
//El mètodo listen puede omitir la callback: app.listen(3000); con ello,
//solamente ya no se mostrarà el msj en la console



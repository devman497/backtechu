const global = require('../global');
const data = require('../user.json');
const bodyParser = require('body-parser');


//LOGIN users
function Login(req, res) {
    console.log("Login");
    console.log(req.body.email);
    console.log(req.body.password);
    let pos = 0;
    let flag_exits = false;
    let idUsuario = 0;
    while (pos < data.length && !flag_exits) {
      if (
        data[pos].email == req.body.email &&
        data[pos].password == req.body.password
      ) {
        idUsuario = data[pos].ID;
        data[pos].logged = true;
        writeFile(data[pos]);
        flag_exits = true;
      }
      pos++;
    }
    if (flag_exits) {
      //fue encontrado
      res.send({
        Encontrado: "Sì",
        id: data[pos - 1]
      });
    } else {
      res.send({
        Encontrado: "No"
      });
    }
  }
  //LOGOUT users
function Logout(req, res) {
    console.log("Logout");
    console.log(req.params.id);
    console.log(req.body.email);
    console.log(req.body.password);
    let flag_exits = false;
    let idUsuario = 0;
    let pos = req.params.id - 1;
    if (data[pos].logged == true) {
      data[pos].logged = false;
      idUsuario = data[pos];
      flag_exits = true;
    }
    if (flag_exits) {
      //fue encontrado
      res.send({
        "Logout Success": "Sì",
        id: data[pos - 1]
      });
    } else {
      res.send({
        "User not Login": "401"
      });
    }
  }
  function writeFile(data) {
    const fs = require("fs");
    var jsonData = JSON.stringify(data);
    fs.writeFile("../userLogin.json", jsonData, "utf8", function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log("JSON generated!");
      }
    });
  }


  module.exports = {
      Login,
      Logout
  }
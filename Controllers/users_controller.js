var express = require('express');
var data = require('../user.json');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
const global = require('../global');
app.use(bodyParser.json());

function getQuerys(req, res) {
  console.log(req.query.id);
  console.log(req.query.first_name);
  res.send(data[req.query.id-1]);
}


// function GET mostrar todo los usuarios
function getUsers(req, res) {
  //res.send({"msg":"Operation GET"});
  res.send(data);
};

function getById(req, res) {
  //users/:id/:otroparametro/:yotro/:etc
  console.log("GET users ID " + req.params.id);
  // console.log("GET users ID "+ req.params.otroparametro);
  // console.log("GET users ID "+ req.params.yotro);
  let pos = req.params.id - 1;
  let rspta = data[pos];
  //let rsta =  (data [pos] == undefined ) ? {"msg": "usuario no encontrado"} . data [ pos];
  if (rspta != undefined) {
    res.send(rspta);
  } else {
    res.send("Element Not Found");
  }
};


module.exports = {
  getQuerys,
  getById,
  getUsers
}
